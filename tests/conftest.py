# -*- coding: utf-8 -*-
import pytest

import settings
from src.embedding import FastTextAPI


@pytest.yield_fixture(scope='module')
def service():
    yield FastTextAPI(settings.FAST_TEXT_MODEL_PATH)
