# -*- coding: utf-8 -*-
from src.service_functions import get_numpy_vector


class TestServer:

    def test_words_embedding(self, service):
        with service.app.test_client() as client:
            response = client.post('/api/v1', json={
                'jsonrpc': '2.0',
                'method': 'words_embedding',
                'params': ['pen', 'red', 'tip'],
                'id': '1'
            })
            vector = get_numpy_vector(response.get_json())
            assert (len(vector) == 3)
            for item in vector:
                assert (len(item) == 300)

    def test_sentences_embedding(self, service):
        with service.app.test_client() as client:
            response = client.post('/api/v1', json={
                'jsonrpc': '2.0',
                'method': 'sentences_embedding',
                'params': ['We are programmers', 'Writing nice code',
                           'With useful docs', 'Except from others'],
                'id': '1'
            })
            vector = get_numpy_vector(response.get_json())
            assert (len(vector) == 4)
            for item in vector:
                assert (len(item) == 300)
