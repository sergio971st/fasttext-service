# -*- coding: utf-8 -*-
from flask import Flask
from jsonrpc.backend.flask import api
from fastText import load_model

from src.service_functions import serialize_vector
from src.jsonrpc_exceptions import ValidationError
import settings


class FastTextAPI:

    def __init__(self, fast_text_model_path, host=settings.HOST,
                 port=settings.PORT):
        self.ftext = load_model(fast_text_model_path)
        self.port = port
        self.host = host
        self.app = Flask(__name__, static_url_path='')
        self.app.add_url_rule(
            rule='/api/v1', view_func=api.as_view(), methods=['POST'])
        api.dispatcher.add_method(
            self.sentences_embedding, 'sentences_embedding')
        api.dispatcher.add_method(self.words_embedding, 'words_embedding')

    def words_embedding(self, *words):
        """
        Method that returns you words embeddings
        :param words: list of words
        :return: hashed list of embeddings (example of unhashing is in README)
        """

        if len(words) > 500:
            raise ValidationError('Length of words list must be less than 500')

        words_emb = []
        for word in words:
            words_emb.append(self.ftext.get_word_vector(word))

        return serialize_vector(words_emb)

    def sentences_embedding(self, *sentences):
        """
        Method that returns you sentences embeddings
        :param sentences: list of sentences
        :return: hashed list of embeddings (example of unhashing is in README)
        """

        if len(sentences) > 100:
            raise ValidationError(
                'Length of sentences list must be less than 100')

        sentences_emb = []
        for sentence in sentences:
            sentences_emb.append(self.ftext.get_sentence_vector(sentence))

        return serialize_vector(sentences_emb)

    def run(self):
        self.app.run(self.host, self.port)
