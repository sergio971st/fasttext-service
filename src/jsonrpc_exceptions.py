# -*- coding: utf-8 -*-
from jsonrpc.exceptions import JSONRPCDispatchException


class JSONRPCExceptionBase(JSONRPCDispatchException):

    def __init__(self, key=None, explanation=None, **kwargs):
        super().__init__(
            code=self.code,
            message=self.message,
            data=dict(key=key, explanation=explanation, **kwargs)
        )


class ValidationError(JSONRPCExceptionBase):
    code = 2000
    message = 'Validation error'
