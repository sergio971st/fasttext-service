# -*- coding: utf-8 -*-
import msgpack_numpy
import base64


def get_numpy_vector(result):
    return msgpack_numpy.loads(base64.b64decode(result['result']))


def serialize_vector(vector):
    return base64.b64encode(msgpack_numpy.dumps(vector)).decode()
