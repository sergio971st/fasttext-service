# Fasttext Service

### DEVELOPMENT:
1. Venv + установка зависимостей:
```
virtualenv -p python3.7 venv
. venv/bin/activate
pip install -r requirements-dev.txt
cp settings/local.py.default settings/local.py
```
2. Установка pre-commit:
```
pre-commit install
```
3. Локальный запуск:
```
python manage.py run
```
4. Запуск тестов:
```
python manage.py test
```

### USEFUL COMMANDS
1. Собрать docker
```
sudo docker build -t fast_text_service .
```
2. Запустить docker
```
sudo docker run -p 5000:5000 -t -i fast_text_service
```
3. Для просмотра содержимого запущенного docker-контейнера:
```
docker exec -it <dockername> bash
```


### EXAMPLE OF USAGE:
```
import requests
from src.service_functions import get_numpy_vector

data = json.dumps({
    'jsonrpc': '2.0',
    'method': 'words_embedding',
    'params': ['pen', 'red', 'tip'],
    'id': '1'
})
headers = {'Content-Type': 'application/json'}
response = requests.post('http://localhost:5000/api/v1', data=data, headers=headers)

vector = get_numpy_vector(response.json())
```
