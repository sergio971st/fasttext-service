flask==1.0.2
gunicorn==19.9.0
scipy==1.2.1
git+https://github.com/facebookresearch/fastText.git#egg=fasttext-0.8.22
json-rpc==1.12.1
msgpack-numpy==0.4.4.2
pytest==4.4.1
coverage==4.5.3
pytest-cov==2.6.1
