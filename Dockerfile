FROM python:3.7

EXPOSE 5000

COPY . /root

WORKDIR /root

RUN pip install -r requirements-dev.txt

RUN cp settings/local.py.docker settings/local.py

ENTRYPOINT ["python", "manage.py"]
CMD ["run"]
