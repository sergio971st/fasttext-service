# -*- coding: utf-8 -*-
import sys
import subprocess

import click

import settings

from src.embedding import FastTextAPI


@click.group()
def cli():
    pass


@cli.command()
def run():
    service = FastTextAPI(settings.FAST_TEXT_MODEL_PATH)
    service.run()


@cli.command()
@click.option('--coverage', is_flag=True)
def test(coverage):
    args = ['pytest', 'tests']
    if coverage:
        args.append('--cov=src')
    completed_process = subprocess.run(args)
    sys.exit(completed_process.returncode)


if __name__ == '__main__':
    cli()
